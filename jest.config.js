module.exports = {
    preset: 'ts-jest',
    testEnvironment: "node",
    transform: {
        "^.+\\.tsx?$": "ts-jest"
    },
    collectCoverage: false,
    coverageDirectory: "coverage",
    coverageReporters: [
        // "lcov",
        "text"
    ],
    collectCoverageFrom: [
        "src/**/*.ts",
        "!src/**/*.spec.ts",
    ],
    testMatch: [
        "<rootDir>/src/**/*.spec.ts",
    ],
    moduleFileExtensions: [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json"
    ],
    modulePathIgnorePatterns: [
        "<rootDir>/(dist|docs|node_modules)/"
    ],
    transformIgnorePatterns: [
        "<rootDir>/node_modules/",
    ],
    globals: {
        "ts-jest": {
            diagnostics: false,
            isolatedModules: true,
            tsConfig: {
                "target": "esnext"
            }
        }
    },
    moduleNameMapper: {
        '^factory$': '<rootDir>/src/factory',
        '^factory/(.*)$': '<rootDir>/src/factory/$1',
        '^object$': '<rootDir>/src/object',
        '^object/(.*)$': '<rootDir>/src/object/$1',
        '^unittest$': '<rootDir>/src/unittest',
        '^unittest/(.*)$': '<rootDir>/src/unittest/$1',
    },
    modulePaths: [
        "<rootDir>/src/factory/",
        "<rootDir>/src/object/",
        "<rootDir>/src/unittest/"
    ]
};
