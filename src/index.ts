export * from 'factory';
export * from 'factory/injection';
export * from 'factory/providers';

import InjectionFactory from 'factory';
export default InjectionFactory;
