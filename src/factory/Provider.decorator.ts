import InjectionFactory from 'factory';
import { InjectionMetadata } from 'factory/injection/InjectionMetadata';
import { InjectionProvider } from 'factory/injection/InjectionProvider';
import { InjectionToken, Token } from 'factory/injection/InjectionToken';
import 'reflect-metadata';
import { ConstructorFunction, Nullable } from 'simplytyped';


/**
 * The provider target configuration.
 */
export interface ProviderMetadata<T> extends InjectionMetadata
{
    token: Token<T>
}

export function Provider<T>(metadata: ProviderMetadata<T>)
{
    return <Type extends ConstructorFunction<InjectionProvider<T>>>(constructor: Type) =>
    {
        InjectionFactory.RegisterProvider<T>(InjectionToken.Normalize(metadata.token), constructor, metadata.scope);

        return constructor;
    };
}

//region get/set ProviderMetadata

const PROVIDER_METADATA_KEY          = Symbol('PROVIDER_KEY'),
      PROVIDER_METADATA_PROPERTY_KEY = `PROVIDER_METADATA`;

export function getProviderMetadata<T extends object>(target: ConstructorFunction<T>)
{
    return Reflect.getMetadata(
        PROVIDER_METADATA_KEY,
        target,
        PROVIDER_METADATA_PROPERTY_KEY,
    ) as Nullable<ProviderMetadata<T>>;
}

export function setProviderMetadata<T extends object>(target: ConstructorFunction<T>, token: Token)
{
    Reflect.defineMetadata(PROVIDER_METADATA_KEY, { token: token }, target, PROVIDER_METADATA_PROPERTY_KEY);
}

//endregion
