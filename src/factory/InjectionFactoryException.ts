import { Exception, TemplateString, TemplateStringValues } from '@awesome-nodes/object';


/**
 * Represents the default exception domain for the {@link InjectionFactory}.
 */
export class InjectionFactoryException extends Exception
{
    public constructor(
        message: TemplateString,
        messageTemplateValues?: TemplateStringValues,
        private parent?: Error)
    {
        super(message, messageTemplateValues, parent);

        InjectionFactoryException.setPrototype(this);
    }
}

