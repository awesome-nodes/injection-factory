import { isPrototypeOf, TemplateString } from '@awesome-nodes/object';
import InjectionFactory from 'factory';
import { InjectionMetadata } from 'factory/injection/InjectionMetadata';
import {
    AnyProvider,
    DependencyProviderType,
    InjectableProvider,
    InjectionProvider,
} from 'factory/injection/InjectionProvider';
import { isProviderConfig } from 'factory/injection/InjectionProviderConfig';
import { InjectionProviderException } from 'factory/injection/InjectionProviderException';
import { InjectionScope } from 'factory/injection/InjectionScope';
import { InjectionToken, SimpleToken, Token } from 'factory/injection/InjectionToken';
import { UnknownInjectionTokenScopeException } from 'factory/injection/InjectionTokenException';
import { getProviderMetadata } from 'factory/Provider.decorator';
import { ClassProvider } from 'factory/providers/ClassProvider';
import { convertFromProviderConfig } from 'factory/providers/ProviderConfig';
import { ConstructorFor, ConstructorFunction, Nullable } from 'simplytyped';


/**
 * The injectable dependency injection configuration.
 */
export interface InjectableMetadata<T extends object> extends InjectionMetadata
{
    /**
     * The injection token to be used as unique identifier for the injectable.
     */
    token?: SimpleToken
    /**
     * A list of providers of inject tokens used within the injectable constructor.
     * The order of the providers within the array reflects the order of injected parameters.
     */
    providers?: Array<InjectableProvider<unknown>>
    //noinspection JSValidateJSDoc T is not recognized
    /**
     * An event handler function used to intercept object instantiation within the injectable constructor.
     * @param {T} instance
     * @param {[]} args The resolved injectable parameter list for the constructor.
     */
    construct?: (instance: T, ...args: unknown[]) => void
}

//region get/set Metadata

export const INJECTABLE_METADATA_KEY          = Symbol('INJECTABLE_KEY'),
             INJECTABLE_METADATA_PROPERTY_KEY = `INJECTABLE_METADATA`;

/**
 * Returns possibly added reflection metadata for the specified target type.
 * @param {ConstructorFor<T>} target
 * @returns {InjectableMetadata | undefined}
 */
export function getInjectableMetadata<T extends object>(target: ConstructorFunction<T>)
{
    return Reflect.getMetadata(
        INJECTABLE_METADATA_KEY,
        target,
        INJECTABLE_METADATA_PROPERTY_KEY,
    ) as Nullable<InjectableMetadata<T>>;
}

export function setInjectableMetadata<T extends object>(target: ConstructorFunction<T>, data: InjectableMetadata<T>)
{
    Reflect.defineMetadata(INJECTABLE_METADATA_KEY, data, target, INJECTABLE_METADATA_PROPERTY_KEY);
}

//endregion

/**
 * Registers the decorated type as injectable within the injection factory facility.
 * @param {InjectableMetadata} metadata
 * @returns {ConstructorFor<T>)}
 */
export function Injectable<T extends object>(metadata?: InjectableMetadata<T>)
{
    return <Type extends ConstructorFunction<T>>(constructor: Type) =>
    {
        const token          = metadata && metadata.token ? metadata.token : constructor.name,
              construct      = metadata && metadata.construct ? metadata.construct : undefined,
              scope          = (metadata && metadata.scope) ? metadata.scope : InjectionFactory.MainScope,
              injectionScope = (scope instanceof InjectionScope)
                  ? scope
                  : InjectionFactory.LocateScope(scope) as InjectionScope,
              providers      = metadata ? metadata.providers : undefined;

        let newConstructor = constructor;

        if (!injectionScope)
            throw new UnknownInjectionTokenScopeException(scope, token, 'Dependency inject failed. ');

        InjectionFactory.RegisterToken(token, scope);

        // Generate InstanceProvider instances out of every object provider which provides its own type.
        providers && providers.forEach((_provider: InjectableProvider<unknown>) =>
        {
            let providerType: DependencyProviderType<unknown>;

            if (!isPrototypeOf(_provider, InjectionProvider)) {

                let providerToken: Nullable<Token>;
                let providerInstance: InjectionProvider<unknown>;
                let providerScope;

                if (isProviderConfig(_provider)) {
                    providerScope    = injectionScope.AddSubscope(InjectionToken.ToString(token));
                    providerInstance = convertFromProviderConfig(_provider, providerScope);
                    providerType     = providerInstance.constructor as DependencyProviderType<T>;
                    providerToken    = injectionScope.LocateToken(providerInstance.Token);
                    if (!providerToken)
                        throw new InjectableDependencyNotFoundException(injectionScope, providerType, token);
                }
                else {
                    providerScope          = injectionScope.AddSubscope(InjectionToken.ToString(token));
                    providerType           = _provider;
                    const providerMetadata = getProviderMetadata(providerType);
                    providerToken          = injectionScope.LocateToken(providerMetadata ? providerMetadata.token : providerType.name);
                    if (!providerToken)
                        throw new InjectableDependencyNotFoundException(injectionScope, providerType, token);

                    if (providerType == constructor)
                        providerType = newConstructor = CreateConstructor(providerType) as Type;

                    providerInstance = new ClassProvider<object>(providerScope, providerToken, providerType);
                }
                providerScope.AddProvider(InjectionToken.Normalize(providerInstance.Token), providerInstance);
            }
        });

        setInjectableMetadata(constructor, metadata as InjectableMetadata<T>);

        /**
         * Creates an overridden constructor only once used to provide `InjectableMetadata.construct` events.
         * @param {Ctor} constructor
         * @returns {Ctor}
         */
        function CreateConstructor<Ctor extends ConstructorFor<{}>, CtorType extends InstanceType<Ctor>>(constructor: Ctor)
        {
            if (construct) {
                if (newConstructor == (constructor as CtorType)) {
                    return class extends constructor
                    {
                        constructor(...args: any[])
                        {
                            super(...args);
                            construct && construct(this as unknown as T, ...args);
                        }
                    };
                }
                else return newConstructor;
            }
            return constructor;
        }

        return CreateConstructor(constructor) as Type;
    };
}

export class InjectableDependencyNotFoundException<T> extends InjectionProviderException<T>
{
    public constructor(scope: InjectionScope, provider: DependencyProviderType<T>, token: Token<T>)
    {
        super(scope, token, provider as AnyProvider<T>, TemplateString`Can not determine dependencies of injectable '${
            'token'
        }'. The provider of dependency '${
            'provider'
        }' has no matching injection token registered within scope ${'scope'}! `);

        InjectableDependencyNotFoundException.setPrototype(this);
    }
}
