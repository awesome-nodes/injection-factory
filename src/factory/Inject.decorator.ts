import { InjectionMetadata } from 'factory/injection/InjectionMetadata';
import { Scope } from 'factory/injection/InjectionScope';
import { SimpleToken, Token } from 'factory/injection/InjectionToken';
import { InjectionFactoryException } from 'factory/InjectionFactoryException';
import 'reflect-metadata';
import { ConstructorFunction, Nullable } from 'simplytyped';


/**
 * Injectable constructor parameter decorator.
 * @param {Token<T>} token Overrides the injection token to be used instead of the decorated argument type.
 * @param {Scope} scope If provided it overrides the source scope to be used for provider lookups for the token.
 * Can be either the scope name located within the parent scope of the injectable or within the
 * {@link InjectionFactory.MainScope} or a fully qualified scope name (scope/*)
 * @returns {(target: object, _: SimpleToken, index: number) => void}
 */
export function Inject<T>(token?: Token<T>, scope?: Scope)
{
    return function (target: object, _: SimpleToken, index: number)
    {
        setInjectionToken(target, {
            scope: scope,
            token: token,
        }, index);
    };
}

//region get/set InjectionToken

const INJECT_METADATA_KEY = Symbol('INJECT_KEY'),
      REFLECT_PARAMS      = 'design:paramtypes';

/**
 * Internal token configuration used to determine available inject configuration.
 */
interface InjectMetadata<T> extends InjectionMetadata
{
    token?: Token<T>
}

export function setInjectionToken<T>(target: object, metadata: InjectMetadata<T>, index: number)
{
    Reflect.defineMetadata(INJECT_METADATA_KEY, metadata, target, `index-${index}`);
}

export function getInjectionToken<T>(target: object, index: number)
{
    return Reflect.getMetadata(INJECT_METADATA_KEY, target, `index-${index}`) as Nullable<InjectMetadata<T>>;
}

//endregion

//region get InjectedParams

/**
 * Represents a type constructor parameter setup which can be used within an injection provider to map tokens.
 */
export interface InjectParam<T = unknown> extends InjectionMetadata
{
    /**
     * The index of the parameter within constructor parameter list.
     */
    index: number,
    /**
     * The inject token.
     */
    token: Token<T>,
    /**
     * Indicates if the parameter was decorated using the `@Inject` decorator.
     */
    inject: boolean
}

/**
 * Returns the parameter setup for the specified target type.
 * @param {ConstructorFunction<T>} target
 * @returns {Array<InjectParam>}
 */
export function getInjectedParams<T extends object>(target: Function): Array<InjectParam<T>>
{
    const argTypes = Reflect.getMetadata(REFLECT_PARAMS, target) as (ConstructorFunction<T> | undefined)[];
    if (argTypes === undefined)
        return [];

    return argTypes.map((argType, index) =>
    {
        // The reflect-metadata API fails on circular dependencies, and will return undefined
        // for the argument instead.
        if (argType === undefined) {
            throw new InjectionFactoryException(`Injection error. Recursive dependency detected in constructor for type ${
                target.name
            } with parameter at index ${index}`);
        }

        const injectionMetadata = getInjectionToken(target, index) as InjectMetadata<T>,
              overrideToken     = injectionMetadata && injectionMetadata.token,
              actualToken       = overrideToken === undefined ? argType : overrideToken;

        return {
            scope : injectionMetadata && injectionMetadata.scope,
            index : index,
            token : actualToken,
            inject: !!injectionMetadata,
        };
    });
}

//endregion
