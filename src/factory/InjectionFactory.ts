import { ArgumentNullException, isString, ObjectBase, StaticConstructable } from '@awesome-nodes/object';
import { AnyProvider, InjectionProvider } from 'factory/injection/InjectionProvider';
import { InjectionProviderConfig, isProviderConfig } from 'factory/injection/InjectionProviderConfig';
import { InjectionScope, Scope } from 'factory/injection/InjectionScope';
import { InjectionToken, ReflectiveToken, Token } from 'factory/injection/InjectionToken';
import { UnknownInjectionTokenScopeException } from 'factory/injection/InjectionTokenException';
import { convertFromProviderConfig } from 'factory/providers/ProviderConfig';
import { Nullable } from 'simplytyped';


const DEFAULT_SCOPE = 'main';

@StaticConstructable
export default class InjectionFactory extends ObjectBase
{
    //region Private Members
    private static _Scopes: Array<InjectionScope> = [];

    //endregion

    public static construct()
    {
        this._Scopes.push(new InjectionScope(DEFAULT_SCOPE));
    }

    //region Injection Scope

    public static get MainScope()
    {
        return this._Scopes[0];
    }

    public static get Scopes(): Array<InjectionScope>
    {
        return this._Scopes.slice();
    }

    /**
     * Try's to locate the specified scope within this injection factory. It locates child-scopes of the main-scope
     * by their scope name. All other scope names must have their exact parent scope chain defined,
     * separated with forward slashes `/`, in order to be able to perform any scope lookup.
     * @param {Scope} scope
     * @param parentScope
     * @returns {InjectionScope | undefined}
     */
    public static LocateScope(scope: Scope, parentScope?: Nullable<Scope>)
    {
        if (!scope || (isString(scope) && !scope.length))
            throw new ArgumentNullException('Can not locate empty scope names. ', 'scope');

        let s = InjectionScope.ToString(scope, parentScope);
        return this._Scopes.find(_scope => String(_scope) == s);
    }

    /**
     * Returns an existing scope instance which equals the provided injection scope name.
     * @param {string} scope The injection scope name.
     * @param {Scope} parentScope The parent scope of the injection scope if any.
     * If 'undefined' is specified, the scope becomes a sub-scope of {@link InjectionFactory.MainScope}.
     * If 'null' is specified, the scope becomes a new isolated root scope.
     * @returns {InjectionScope}
     */
    public static CreateScope<U extends InjectionScope>(scope: string, parentScope?: Nullable<Scope>)
    {
        let injectionScope: InjectionScope;

        if (!scope || !scope.length)
            throw new ArgumentNullException(`Can not create dependency injection scope. `, 'scope');

        const foundScope = this.LocateScope(parentScope
            ? `${parentScope}/${scope}`
            : scope, parentScope);

        if (!foundScope) {
            let parent: Nullable<InjectionScope>;
            parent = parentScope
                ? (parentScope instanceof InjectionScope)
                    ? parentScope
                    : this.CreateScope(parentScope)
                : parentScope === null ? undefined : this.MainScope;

            this._Scopes.push(injectionScope = new InjectionScope(scope, parent));
        }
        else injectionScope = foundScope;

        return injectionScope as U;
    }

    //endregion

    //region Injectable Registration

    /**
     * Registers a new injection token within the provided scope.
     * Scope and parenScope will be created automatically in case if one of them do not exist.
     * Note: Token registration can be done only once to prevent outdated ghost injection sources.
     * @param {ReflectiveToken} token
     * @param {Scope} scope
     * @param {Scope} parentScope
     * @throws {@link DuplicateInjectionTokenException}
     */
    public static RegisterToken<T>(token: ReflectiveToken, scope: Scope = this.MainScope, parentScope?: Scope)
    {
        const injectionScope = scope instanceof InjectionScope ? scope : this.CreateScope(scope, parentScope);
        return injectionScope.AddToken(InjectionToken.Simplify(token)) as InjectionToken<T>;
    }

    /**
     * Registers a new injection provider for the provided token within the provided scope.
     * Scope and parenScope will be created automatically in case if one of them do not exist.
     * Note: Provider registration can be done only once to prevent unwanted accidental overriding. Injection
     * providers can be overridden by calling the {@link InjectionScope.AddProvider} function of the scope directly.
     * @param {Token<T>} token
     * @param {AnyProvider<T>} provider
     * @param {Scope} scope
     * @param {Scope} parentScope
     * @throws {@link ArgumentException} | {@link UnknownInjectionProviderTokenException} | {@link DuplicateInjectionProviderException}
     */
    public static RegisterProvider<T>(
        token: Token | InjectionProviderConfig<T>,
        provider: AnyProvider<T> = (isProviderConfig<T>(token as AnyProvider<T>) && token) as AnyProvider<T>,
        scope: Scope             = (isProviderConfig<T>(provider) && provider.scope) || this.MainScope,
        parentScope?: Nullable<Scope>)
    {
        let injectionScope = scope instanceof InjectionScope
            ? scope
            : this.CreateScope(scope, isProviderConfig<T>(provider) ? null : parentScope);

        if (!provider)
            throw new ArgumentNullException(`Can not add provider for token '${String(token)}'. `, 'provider');

        return injectionScope.AddProvider(
            InjectionToken.Normalize(token as Token),
            provider instanceof InjectionProvider
                ? provider
                : (isProviderConfig(provider)
                    ? convertFromProviderConfig(provider, injectionScope)
                    : new ((provider as Function)!.bind(null, ...[injectionScope, token]))
                ),
        );
    }

    //endregion

    //region Injectable Injection

    /**
     * Resolves the specified injection token within the provided scope.
     * @param {Token<T>} token
     * @param {Scope} scope
     * @returns {T}
     * @throws {@link ArgumentException} | {@link UnknownInjectionTokenScopeException} | {@link UnknownInjectionTokenException} | {@link UnknownInjectionProviderException}
     */
    public static ResolveToken<T>(token: Token<T>, scope: Scope = this.MainScope): T
    {
        const _scope = this.LocateScope(scope);

        if (!_scope)
            throw new UnknownInjectionTokenScopeException(scope, token, 'Injection token lookup failed. ');

        return _scope.Inject(token);
    }

    //endregion
}
