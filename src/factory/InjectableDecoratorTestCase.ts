import { ObjectBase } from '@awesome-nodes/object';
import InjectionFactory from 'factory';
import { Injectable } from 'factory/Injectable.decorator';
import {
    DependencyClass1,
    DependencyClass2,
    InjectableClass,
    InjectionProviderTestCase,
} from 'factory/injection/InjectionProviderTestCase';
import { InjectionScope } from 'factory/injection/InjectionScope';
import { Provider } from 'factory/Provider.decorator';
import { ClassProvider } from 'factory/providers/ClassProvider';


export class InjectableDecoratorTestCase extends InjectionProviderTestCase
{
    protected get DependencyClass1Token()
    {
        return DecoratedDependencyClass1;
    }

    protected get DependencyClass2Token()
    {
        return DecoratedDependencyClass2;
    }

    protected get InjectableClassToken()
    {
        return DecoratedInjectableClass;
    }

    constructor(Scope?: string | InjectionScope)
    {
        super(Scope);
    }

    public BeforeAll(): void
    {
    }
}

const InjectableDecoratorTestCaseScope = InjectionFactory.CreateScope(InjectableDecoratorTestCase.name);

@Injectable()
export class DecoratedDependencyClass1 extends DependencyClass1
{
}

@Injectable()
export class DecoratedDependencyClass2 extends DependencyClass2
{
}

@Provider({
    token: DecoratedDependencyClass2,
    scope: InjectableDecoratorTestCaseScope,
})
class DependencyClass2Provider extends ClassProvider<DecoratedDependencyClass2>
{
    constructor(scope: string, token: string)
    {
        super(scope, token, DecoratedDependencyClass2);
    }
}

@Injectable({
    scope: InjectableDecoratorTestCaseScope,
})
export class DecoratedDependencyClass3 extends ObjectBase
{
    constructor(private readonly _Injector: InjectionScope)
    {
        super();
    }
}

@Injectable({
    scope    : InjectableDecoratorTestCaseScope,
    providers: [
        DecoratedDependencyClass1,
        DependencyClass2Provider,
        {
            provide: DecoratedDependencyClass3,
        },
    ],
})
export class DecoratedInjectableClass extends InjectableClass
{
    constructor(
        dep1: DecoratedDependencyClass1,
        dep2: DecoratedDependencyClass2,
        injector: InjectionScope,
        public dep3: DecoratedDependencyClass3)
    {
        super(dep1, dep2, injector);
    }
}

@Provider({
    scope: InjectableDecoratorTestCaseScope,
    token: DecoratedInjectableClass,
})
class DecoratedInjectableClassProvider extends ClassProvider<DecoratedInjectableClass>
{
    constructor(scope: string, token: string)
    {
        //TODO: Figure out if an implemented injectionProvider should be deny the injection of
        // DecoratedDependencyClass3 if it is not part defined dependencies within this provider constructor.
        super(scope, token, DecoratedInjectableClass, DecoratedDependencyClass1, DecoratedDependencyClass2);
    }
}

