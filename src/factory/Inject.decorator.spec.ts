import { ObjectBase } from '@awesome-nodes/object';
import { itShould } from '@awesome-nodes/unittest';
import InjectionFactory from 'factory';
import { Inject } from 'factory/Inject.decorator';
import { Injectable } from 'factory/Injectable.decorator';
import {
    DecoratedDependencyClass1,
    DecoratedDependencyClass2,
    DecoratedDependencyClass3,
    DecoratedInjectableClass,
    InjectableDecoratorTestCase,
} from 'factory/InjectableDecoratorTestCase';
import { InjectionScope, Scope } from 'factory/injection/InjectionScope';


class InjectDecoratorTestCase extends InjectableDecoratorTestCase
{
    constructor()
    {
        super(InjectDecoratorTestCaseScope);
    }

    public Test(): void
    {
        super.Test();
        itShould(`inject injectable`, () =>
        {
            const instance = this._Scope.Inject<InjectTest>('InjectTest');

            expect(instance).toBeInstanceOf(InjectTest);
            expect(instance.injectableClass).toBeInstanceOf(DecoratedInjectableClass);
            expect(instance.dep1).toBeInstanceOf(DecoratedDependencyClass3);
            expect(instance.injector).toBeInstanceOf(InjectionScope);
            expect(instance.dep2).toBeInstanceOf(DecoratedDependencyClass2);
            expect(instance.dep3).toBeInstanceOf(DecoratedDependencyClass1);
        });
    }
}

const InjectDecoratorTestCaseScope = InjectionFactory.LocateScope(InjectableDecoratorTestCase.name) as Scope;

interface IDecoratedDependencyClass3 extends DecoratedDependencyClass3
{
}

@Injectable({
    scope    : InjectDecoratorTestCaseScope,
    providers: [InjectTest, DecoratedDependencyClass1],
    construct: (instance: InjectTest) =>
    {
        console.log(`DI: ${instance} is created.\n`);
    },
})
class InjectTest extends ObjectBase
{
    constructor(
        @Inject() public injectableClass: DecoratedInjectableClass,
        @Inject(DecoratedDependencyClass3, DecoratedInjectableClass.name) public dep1: DecoratedDependencyClass1,
        public injector: InjectionScope,
        public dep2: DecoratedDependencyClass2,
        @Inject(DecoratedDependencyClass1) public dep3: IDecoratedDependencyClass3)
    {
        super();
    }
}

new InjectDecoratorTestCase;
