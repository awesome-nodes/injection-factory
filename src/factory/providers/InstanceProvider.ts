import { TemplateString } from '@awesome-nodes/object';
import InjectionFactory from 'factory';
import { getInjectedParams } from 'factory/Inject.decorator';
import { getInjectableMetadata, InjectableMetadata } from 'factory/Injectable.decorator';
import { AnyProvider, InjectionProvider } from 'factory/injection/InjectionProvider';
import { UnknownInjectionProviderTokenException } from 'factory/injection/InjectionProviderException';
import { InjectionScope, Scope } from 'factory/injection/InjectionScope';
import { InjectionToken, Token } from 'factory/injection/InjectionToken';
import { ConstructorFunction, Nullable } from 'simplytyped';


export abstract class InstanceProvider<T extends object> extends InjectionProvider<T>
{
    protected _Dependencies: Array<Token>;

    public get Dependencies(): Array<Token>
    {
        return this._Dependencies;
    }

    protected constructor(
        scope: Scope,
        token: Token,
        protected _Constructor: Function,
        ...dependencies: Array<Token>)
    {
        super(scope, token, null);

        this._Dependencies = dependencies;
    }

    //region Resolve Injectable Dependencies

    private ResolveInjectDependencies()
    {
        let dependencies: unknown[] = [];

        for (const _param of getInjectedParams(this._Constructor)) {
            if (typeof _param.token != 'function')
                continue;

            if (!_param.inject || this._Dependencies.some(_dep => InjectionToken.Compare(_dep, _param.token))) {
                dependencies.push(_param.token == InjectionScope ? this._Scope : this._Scope.Inject(_param.token));
                continue;
            }
            let injectableMetadata = getInjectableMetadata(_param.token as ConstructorFunction<T>) as InjectableMetadata<T>;
            let tokenScope: Scope;
            if (!injectableMetadata) {
                // If not decorated use scope from injection token and also verify token existence
                const token = this._Scope.LocateToken(_param.token);
                if (!token) {
                    throw new UnknownInjectionProviderTokenException(
                        this._Scope,
                        _param.token,
                        this._Constructor as AnyProvider<T>,
                        TemplateString`Can not resolve injection tokens for type '${
                            'provider'
                        }'. The type of the ${
                            'index'
                        }. constructor parameter '${
                            'parameter'
                        } has no token defined.'`,
                        {
                            index    : _param.index + 1,
                            parameter: _param.token.name,
                        },
                    );
                }
                tokenScope = token.Scope;
            }
            else tokenScope = injectableMetadata.scope as Scope;
            let providerScope: Nullable<InjectionScope>;
            if (_param.scope) {
                providerScope = (InjectionFactory.Scopes.find(
                    _scope => _scope == _param.scope || String(_scope) == String(_param.scope))
                    || InjectionFactory.LocateScope(_param.scope));
            }
            else {
                // Try locate providers within this provider scope
                const provider = this._Scope.Providers.find(
                    _provider => InjectionToken.Compare(_provider.Token, _param.token));
                if (provider)
                    providerScope = this._Scope;
            }
            if (!providerScope)
                providerScope = InjectionFactory.LocateScope(tokenScope);
            dependencies.push(providerScope!.Inject(_param.token));
        }

        return dependencies;
    }

    protected ResolveDependencies()
    {
        const dependencies = this.ResolveInjectDependencies();

        // In case if the @Inject decorator has not been used within the target constructor,
        // we resolve the provider dependency list in their defined order.
        if (!dependencies.length && this._Dependencies.length)
            dependencies.push(...this._Dependencies.map(_dep => this._Scope.Inject(_dep)));

        if (!dependencies.some(_dep => _dep == this._Scope))
            dependencies.push(this._Scope);

        return dependencies;
    }

    //endregion

    protected abstract CreateInstance(): T

    public Provide(): T
    {
        return this._Value || (this._Value = this.CreateInstance());
    }
}
