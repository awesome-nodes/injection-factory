import { Scope } from 'factory/injection/InjectionScope';
import { Token } from 'factory/injection/InjectionToken';
import { InstanceProvider } from 'factory/providers/InstanceProvider';
import { ConstructorFunction } from 'simplytyped';


export class ClassProvider<T extends object> extends InstanceProvider<T>
{
    public constructor(scope: Scope, token: Token, value: ConstructorFunction<T>, ...dependencies: Array<Token>)
    {
        super(scope, token, value, ...dependencies);
    }

    protected CreateInstance()
    {
        return Reflect.construct(this._Constructor, this.ResolveDependencies());
    }
}
