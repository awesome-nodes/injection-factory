import { InjectionProvider } from 'factory/injection/InjectionProvider';
import { Scope } from 'factory/injection/InjectionScope';
import { SimpleToken, Token } from 'factory/injection/InjectionToken';


export class TokenProvider<T> extends InjectionProvider<T>
{
    public constructor(scope: Scope, token: Token, private _MappedToken: SimpleToken)
    {
        super(scope, token, null);
    }

    public Provide(): T
    {
        return this._Value || (this._Value = this._Scope.Inject(this._MappedToken) as T);
    }
}
