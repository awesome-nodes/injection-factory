import { ArgumentNullException, DataTypes, TemplateString } from '@awesome-nodes/object';
import InjectionFactory from 'factory';
import { InjectionProvider } from 'factory/injection/InjectionProvider';
import {
    AnyInjectionProviderConfig,
    InjectionProviderConfig,
    isConstructorProvider,
} from 'factory/injection/InjectionProviderConfig';
import { InjectionProviderConfigException } from 'factory/injection/InjectionProviderConfigException';
import { Scope } from 'factory/injection/InjectionScope';
import { InjectionToken, Token } from 'factory/injection/InjectionToken';
import { ClassProvider } from 'factory/providers/ClassProvider';
import { FactoryProvider } from 'factory/providers/FactoryProvider';
import { TokenProvider } from 'factory/providers/TokenProvider';
import { AnyFunc, ConstructorFunction } from 'simplytyped';


/**
 * Resolves and creates an injection provider configuration for the specified token.
 * @param {Token<T>} token
 * @param value If not specified, the token itself is used as the token value.
 * @param {Scope} scope
 * @returns InjectionProviderConfig<T>
 * @throws InjectionProviderConfigException
 */
export function createProviderConfig<T>(
    token: Token<T>,
    value: unknown = token,
    scope: Scope   = InjectionFactory.MainScope)
{
    let provider: InjectionProviderConfig<T>;

    switch (DataTypes.Parse(value)) {
        case DataTypes.Boolean:
        case DataTypes.Number:
        case DataTypes.BigInt:
        case DataTypes.String:
        case DataTypes.Date:
        case DataTypes.RegExp:
        case DataTypes.Array:
        case DataTypes.Object:
        case DataTypes.Error:
        case DataTypes.Instance:
            if (!(value instanceof InjectionToken)) {
                provider = { provide: token, useValue: value as T, scope: scope };
                break;
            }
        //noinspection FallThroughInSwitchStatementJS InjectionToken values are provided by the token provider.
        case DataTypes.Symbol:
            provider = { provide: token, useToken: InjectionToken.Simplify(value as Token), scope: scope };
            break;
        case DataTypes.Type:
            provider = { provide: token, useClass: value as ConstructorFunction<object>, scope: scope };
            break;
        case DataTypes.Function:
            provider = { provide: token, useFactory: value as AnyFunc<T>, scope: scope };
            break;
        case DataTypes.Null:
        case DataTypes.Undefined:
        default:
            throw new ArgumentNullException(
                TemplateString`Can not create provider configuration for token '${InjectionToken.ToString(token)}'. `,
                'value',
            );
    }

    return provider;
}

/**
 * Resolves and creates an injection provider for the specified token.
 * @param {Token<T>} token
 * @param value
 * @param {Scope} scope
 * @returns {InjectionProvider<T>}
 * @throws InjectionProviderConfigException
 */
export function createInjectionProvider<T>(
    token: Token<T>,
    value: unknown = token,
    scope: Scope   = InjectionFactory.MainScope)
{
    let provider: InjectionProvider<unknown>;
    switch (DataTypes.Parse(value)) {
        case DataTypes.Boolean:
        case DataTypes.Number:
        case DataTypes.BigInt:
        case DataTypes.String:
        case DataTypes.Date:
        case DataTypes.RegExp:
        case DataTypes.Array:
        case DataTypes.Object:
        case DataTypes.Error:
        case DataTypes.Instance:
            if (!(value instanceof InjectionToken)) {
                provider = new InjectionProvider<T>(scope, token, value as T);
                break;
            }
        //noinspection FallThroughInSwitchStatementJS InjectionToken values are provided by the token provider.
        case DataTypes.Symbol:
            provider = new TokenProvider<T>(scope, token, InjectionToken.Simplify(value as Token));
            break;
        case DataTypes.Type:
            provider = new ClassProvider<object>(scope, token, value as ConstructorFunction<object>);
            break;
        case DataTypes.Function:
            provider = new FactoryProvider<object>(scope, token, value as AnyFunc<object>);
            break;
        case DataTypes.Null:
        case DataTypes.Undefined:
        default:
            throw new ArgumentNullException(
                TemplateString`Can not create provider configuration for token '${InjectionToken.ToString(token)}'. `,
                'value',
            );
    }
    return provider as InjectionProvider<T>;
}

/**
 * Converts a configuration provider into a known injection provider.
 * @param {InjectionProviderConfig<T>} provider The provider configuration to convert.
 * @param {Scope} scope Overrides the injection scope of the provided provider configuration.
 * @returns {InjectionProvider<T>}
 * @throws InjectionProviderConfigException
 */
export function convertFromProviderConfig<T>(
    provider: InjectionProviderConfig<T>,
    scope: Scope = InjectionFactory.MainScope)
{
    let providerType: ConstructorFunction<InjectionProvider<unknown>>,
        value: unknown;

    const anyProvider = provider as AnyInjectionProviderConfig<T>;

    if ((value = anyProvider.useValue) !== undefined)
        providerType = InjectionProvider;
    else if ((value = anyProvider.useFactory) !== undefined)
        providerType = FactoryProvider;
    else if ((value = anyProvider.useToken) !== undefined)
        providerType = TokenProvider;
    else if (isConstructorProvider(anyProvider) && ((value = anyProvider.provide) || (value = anyProvider.useClass)) !== undefined)
        providerType = ClassProvider;
    else throw new InjectionProviderConfigException(
            provider.scope || scope,
            provider.provide,
            provider,
            TemplateString`Can not convert an unknown provider configuration '${
                'provider'
            }' for token '${'token'}' to a known injection provider.`,
        );

    return new providerType(
        scope != InjectionFactory.MainScope ? scope : (provider.scope || scope),
        provider.provide,
        value,
        ...(provider.deps || []),
    ) as InjectionProvider<T>;
}

