import { itReturns, TestCase } from '@awesome-nodes/unittest';
import InjectionFactory from 'factory';
import { InjectionProvider } from 'factory/injection/InjectionProvider';
import {
    DependencyClass1,
    DependencyClass2,
    InjectableClass,
    InjectionProviderTestCase,
} from 'factory/injection/InjectionProviderTestCase';
import { Scope } from 'factory/injection/InjectionScope';
import { ClassProvider } from 'factory/providers/ClassProvider';
import { FactoryProvider } from 'factory/providers/FactoryProvider';
import {
    convertFromProviderConfig as ConvertFromProviderConfig,
    createInjectionProvider as CreateInjectionProvider,
    createProviderConfig as CreateProviderConfig,
} from 'factory/providers/ProviderConfig';
import { TokenProvider } from 'factory/providers/TokenProvider';
import { ConstructorFunction } from 'simplytyped';


new class ProviderConfigTestCase extends InjectionProviderTestCase
{
    public BeforeAll(): void
    {
        super.BeforeAll();

        InjectionFactory.RegisterProvider(
            DependencyClass1,
            { provide: DependencyClass1 },
            this._Scope,
        );

        InjectionFactory.RegisterProvider(
            DependencyClass2,
            { provide: DependencyClass2 },
            this._Scope,
        );

        InjectionFactory.RegisterProvider(
            InjectableClass,
            {
                provide: InjectableClass,
                deps   : [DependencyClass1, DependencyClass2],
            },
            this._Scope,
        );
    }

    public Test(): void
    {
        super.Test();

        new class createProviderConfig extends TestCase
        {
            constructor(private _Scope?: Scope)
            {
                super();
            }

            private AssertCreateProviderConfig(value: unknown, propertyPath: string)
            {
                const providerConfig = CreateProviderConfig(ProviderConfigTestCase, value, this._Scope);

                expect(providerConfig).toHaveProperty(propertyPath);
            }

            Test()
            {
                //region ValueProviderConfig

                itReturns(`ValueProviderConfig`, 'Boolean', () =>
                {
                    this.AssertCreateProviderConfig(false, 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'Number', () =>
                {
                    this.AssertCreateProviderConfig(0x1, 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'BigInt', () =>
                {
                    this.AssertCreateProviderConfig(0n, 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'String', () =>
                {
                    this.AssertCreateProviderConfig('value', 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'Date', () =>
                {
                    this.AssertCreateProviderConfig(new Date(Date.now()), 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'RegExp', () =>
                {
                    this.AssertCreateProviderConfig(/.*/, 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'Array', () =>
                {
                    this.AssertCreateProviderConfig([], 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'Object', () =>
                {
                    this.AssertCreateProviderConfig({}, 'useValue');
                });

                itReturns(`ValueProviderConfig`, 'Error', () =>
                {
                    this.AssertCreateProviderConfig(new Error('Message'), 'useValue');
                });

                //endregion

                itReturns(`ClassProviderConfig`, 'Constructor', () =>
                {
                    this.AssertCreateProviderConfig(ProviderConfigTestCase, 'useClass');
                });

                itReturns(`FactoryProviderConfig`, 'Function', () =>
                {
                    this.AssertCreateProviderConfig(() =>
                    {
                    }, 'useFactory');
                });

                itReturns(`TokenProviderConfig`, 'Symbol', () =>
                {
                    this.AssertCreateProviderConfig(Symbol(this.Name), 'useToken');
                });
            }
        }(this._Scope);

        new class createInjectionProvider extends TestCase
        {
            constructor(private _Scope?: Scope)
            {
                super();
            }

            private AssertCreateInjectionProvider<T>(
                value: unknown,
                providerType: ConstructorFunction<InjectionProvider<T>>)
            {
                const providerConfig = CreateInjectionProvider(ProviderConfigTestCase, value, this._Scope);

                expect(providerConfig).toBeInstanceOf(providerType);
            }

            Test()
            {
                //region InjectionProvider (ValueProvider)

                itReturns(`InjectionProvider`, 'Boolean', () =>
                {
                    this.AssertCreateInjectionProvider(false, InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'Number', () =>
                {
                    this.AssertCreateInjectionProvider(0x1, InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'BigInt', () =>
                {
                    this.AssertCreateInjectionProvider(0n, InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'String', () =>
                {
                    this.AssertCreateInjectionProvider('value', InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'Date', () =>
                {
                    this.AssertCreateInjectionProvider(new Date(Date.now()), InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'RegExp', () =>
                {
                    this.AssertCreateInjectionProvider(/.*/, InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'Array', () =>
                {
                    this.AssertCreateInjectionProvider([], InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'Object', () =>
                {
                    this.AssertCreateInjectionProvider({}, InjectionProvider);
                });

                itReturns(`InjectionProvider`, 'Error', () =>
                {
                    this.AssertCreateInjectionProvider(new Error('Message'), InjectionProvider);
                });

                //endregion

                itReturns(`ClassProvider`, 'Constructor', () =>
                {
                    this.AssertCreateInjectionProvider(ProviderConfigTestCase, ClassProvider);
                });

                itReturns(`FactoryProvider`, 'Function', () =>
                {
                    this.AssertCreateInjectionProvider(() =>
                    {
                    }, FactoryProvider);
                });

                itReturns(`TokenProvider`, 'Symbol', () =>
                {
                    this.AssertCreateInjectionProvider(Symbol(this.Name), TokenProvider);
                });
            }
        }(this._Scope);

        new class convertFromProviderConfig extends TestCase
        {
            constructor(private _Scope?: Scope)
            {
                super();
            }

            private AssertConvertFromProviderConfig<T>(
                value: unknown,
                providerType: ConstructorFunction<InjectionProvider<T>>)
            {
                const providerConfig    = CreateProviderConfig(ProviderConfigTestCase, value, this._Scope),
                      injectionProvider = ConvertFromProviderConfig(providerConfig, this._Scope);

                expect(injectionProvider).toBeInstanceOf(providerType);
            }

            Test()
            {
                //region ValueProviderConfig

                itReturns(`ValueProviderConfig`, 'Boolean', () =>
                {
                    this.AssertConvertFromProviderConfig(false, InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'Number', () =>
                {
                    this.AssertConvertFromProviderConfig(0x1, InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'BigInt', () =>
                {
                    this.AssertConvertFromProviderConfig(0n, InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'String', () =>
                {
                    this.AssertConvertFromProviderConfig('value', InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'Date', () =>
                {
                    this.AssertConvertFromProviderConfig(new Date(Date.now()), InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'RegExp', () =>
                {
                    this.AssertConvertFromProviderConfig(/.*/, InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'Array', () =>
                {
                    this.AssertConvertFromProviderConfig([], InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'Object', () =>
                {
                    this.AssertConvertFromProviderConfig({}, InjectionProvider);
                });

                itReturns(`ValueProviderConfig`, 'Error', () =>
                {
                    this.AssertConvertFromProviderConfig(new Error('Message'), InjectionProvider);
                });

                //endregion

                itReturns('ClassProvider', 'Class', () =>
                {
                    this.AssertConvertFromProviderConfig(ProviderConfigTestCase, ClassProvider);
                });

                itReturns('FactoryProvider', 'Function', () =>
                {
                    this.AssertConvertFromProviderConfig(() =>
                    {
                    }, FactoryProvider);
                });

                itReturns('TokenProvider', 'Symbol', () =>
                {
                    this.AssertConvertFromProviderConfig(Symbol(this.Name), TokenProvider);
                });
            }
        }(this._Scope);
    }
};
