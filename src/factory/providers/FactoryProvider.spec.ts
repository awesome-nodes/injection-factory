import InjectionFactory from 'factory';
import {
    DependencyClass1,
    DependencyClass2,
    InjectableClass,
    InjectionProviderTestCase,
} from 'factory/injection/InjectionProviderTestCase';
import { InjectionScope } from 'factory/injection/InjectionScope';
import { FactoryProvider } from 'factory/providers/FactoryProvider';


new class FactoryProviderTestCase extends InjectionProviderTestCase
{
    public BeforeAll(): void
    {
        super.BeforeAll();
        let scope = this._Scope;

        InjectionFactory.RegisterProvider(
            DependencyClass1,
            class DependencyClass1Provider extends FactoryProvider<DependencyClass1>
            {
                constructor()
                {
                    super(scope, DependencyClass1, (injector: InjectionScope) =>
                    {
                        return new DependencyClass1(injector);
                    });
                }
            },
            scope,
        );

        InjectionFactory.RegisterProvider(
            DependencyClass2,
            class DependencyClass2Provider extends FactoryProvider<DependencyClass2>
            {
                constructor()
                {
                    super(scope, DependencyClass2, (injector: InjectionScope) =>
                    {
                        return new DependencyClass2(injector);
                    });
                }
            },
            scope,
        );

        InjectionFactory.RegisterProvider(
            InjectableClass,
            class InjectableClassProvider extends FactoryProvider<InjectableClass>
            {
                constructor()
                {
                    super(
                        scope,
                        InjectableClass,
                        (dep1: DependencyClass1, dep2: DependencyClass2, _injector: InjectionScope) =>
                        {
                            return new InjectableClass(dep1, dep2, _injector);
                        },
                        DependencyClass1,
                        DependencyClass2,
                    );
                }
            },
            scope,
        );
    }
};
