import InjectionFactory from 'factory';
import {
    DependencyClass1,
    DependencyClass2,
    InjectableClass,
    InjectionProviderTestCase,
} from 'factory/injection/InjectionProviderTestCase';
import { ClassProvider } from 'factory/providers/ClassProvider';


new class ClassProviderTestCase extends InjectionProviderTestCase
{
    public BeforeAll(): void
    {
        super.BeforeAll();
        let scope = this._Scope;

        InjectionFactory.RegisterProvider(
            DependencyClass1,
            class DependencyClass1Provider extends ClassProvider<DependencyClass1>
            {
                constructor()
                {
                    super(scope, DependencyClass1, DependencyClass1);
                }
            },
            scope,
        );

        InjectionFactory.RegisterProvider(
            DependencyClass2,
            class DependencyClass2Provider extends ClassProvider<DependencyClass2>
            {
                constructor()
                {
                    super(scope, DependencyClass2, DependencyClass2);
                }
            },
            scope,
        );

        InjectionFactory.RegisterProvider(
            InjectableClass,
            class InjectableClassProvider extends ClassProvider<InjectableClass>
            {
                constructor()
                {
                    super(
                        scope,
                        InjectableClass,
                        InjectableClass,
                        DependencyClass1,
                        DependencyClass2,
                    );
                }
            },
            scope,
        );
    }
};
