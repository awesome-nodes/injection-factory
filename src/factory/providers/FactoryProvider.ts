import { Scope } from 'factory/injection/InjectionScope';
import { Token } from 'factory/injection/InjectionToken';
import { InstanceProvider } from 'factory/providers/InstanceProvider';
import { AnyFunc } from 'simplytyped';


export class FactoryProvider<T extends object> extends InstanceProvider<T>
{
    public constructor(scope: Scope, token: Token, factory: AnyFunc<T>, ...dependencies: Array<Token>)
    {
        super(scope, token, factory, ...dependencies);
    }

    protected CreateInstance(): T
    {
        return this._Constructor.bind(null, ...this.ResolveDependencies())();
    }
}
