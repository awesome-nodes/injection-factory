import { TemplateString, TemplateStringValues } from '@awesome-nodes/object';
import { AnyProvider } from 'factory/injection/InjectionProvider';
import { InjectionProviderException } from 'factory/injection/InjectionProviderException';
import { Scope } from 'factory/injection/InjectionScope';
import { Token } from 'factory/injection/InjectionToken';
import { Nullable } from 'simplytyped';


export class InjectionProviderConfigException<T> extends InjectionProviderException<T>
{
    public constructor(
        scope: Scope,
        token: Token<T>,
        provider: Nullable<AnyProvider<T>>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues,
        parent?: Error)
    {
        super(scope, token, provider, message, messageTemplateValues, parent);

        InjectionProviderConfigException.setPrototype(this);
    }
}
