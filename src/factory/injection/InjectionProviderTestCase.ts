import { ObjectBase } from '@awesome-nodes/object';
import { itShould, TestCase, TestLifecycleHooks } from '@awesome-nodes/unittest';
import InjectionFactory from 'factory';
import { InjectionScope, Scope } from 'factory/injection/InjectionScope';
import { InjectionToken, ReflectiveToken } from 'factory/injection/InjectionToken';


const isDebugging = require('debug-mode');

export class DependencyClass1 extends ObjectBase
{
    constructor(public readonly _Injector: InjectionScope)
    {
        super();
    }
}

export class DependencyClass2 extends ObjectBase
{
    constructor(public readonly _Injector: InjectionScope)
    {
        super();
    }
}

export class InjectableClass extends ObjectBase
{
    constructor(
        public dep1: DependencyClass1,
        public dep2: DependencyClass2,
        public _Injector: InjectionScope)
    {
        super();
    }
}

export abstract class InjectionProviderTestCase extends TestCase
{
    protected readonly _Scope: InjectionScope;

    protected get DependencyClass1Token(): ReflectiveToken<DependencyClass1>
    {
        return DependencyClass1;
    }

    protected get DependencyClass2Token(): ReflectiveToken<DependencyClass2>
    {
        return DependencyClass2;
    }

    protected get InjectableClassToken(): ReflectiveToken<InjectableClass>
    {
        return InjectableClass;
    }

    public constructor(_Scope?: Scope)
    {
        super();
        this._Scope = (_Scope && InjectionFactory.LocateScope(_Scope)) || InjectionScope.get(this.Name);
    }

    protected Describe(): void
    {
        this.RegisterLifecycleHook(TestLifecycleHooks.AfterAll, () =>
        {
            if (!isDebugging)
                return;

            for (const _scope of InjectionFactory.Scopes) {
                console.log(
                    `DI: Scope '${_scope}' Tokens -> ` + _scope.Tokens.map((_token) => `\n    ${_token.Name}`),
                    '\n',
                    `DI: Scope '${_scope}' Providers -> ` +
                    _scope.Providers.map((_provider) => `\n    ${_provider.Name}[${ObjectBase.ToString(_provider.Value)}]`),
                );
            }
        });
    }

    public BeforeAll(): void
    {
        InjectionFactory.RegisterToken(this.DependencyClass1Token, this._Scope);
        InjectionFactory.RegisterToken(this.DependencyClass2Token, this._Scope);
        InjectionFactory.RegisterToken(this.InjectableClassToken, this._Scope);
    }

    public Test(): void
    {
        itShould(`inject ${InjectionToken.ToString(this.InjectableClassToken)}`, <T>() =>
        {
            const instance = InjectionFactory.ResolveToken(this.InjectableClassToken, this._Scope);
            expect(instance).toBeInstanceOf(InjectableClass);
        });

        itShould(`inject ${InjectionToken.ToString(this.InjectableClassToken)} dependencies`, <T>() =>
        {
            const instance = InjectionFactory.ResolveToken<InjectableClass>(this.InjectableClassToken, this._Scope);
            expect(instance.dep1).toBeInstanceOf(DependencyClass1);
            expect(instance.dep2).toBeInstanceOf(DependencyClass2);
        });

        itShould(`inject injector (InjectionScope) into all injectables`, <T>() =>
        {
            const instance = InjectionFactory.ResolveToken<InjectableClass>(this.InjectableClassToken, this._Scope);
            expect(instance.dep1._Injector).toBeInstanceOf(InjectionScope);
            expect(instance.dep2._Injector).toBeInstanceOf(InjectionScope);
            expect(instance._Injector).toBeInstanceOf(InjectionScope);
        });
    }
}
