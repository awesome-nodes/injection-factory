import { TemplateString, TemplateStringValues } from '@awesome-nodes/object';
import { AnyProvider, InjectionProvider } from 'factory/injection/InjectionProvider';
import { Scope } from 'factory/injection/InjectionScope';
import { Token } from 'factory/injection/InjectionToken';
import { InjectionTokenException } from 'factory/injection/InjectionTokenException';
import { Nullable } from 'simplytyped';


export class InjectionProviderException<T> extends InjectionTokenException<T>
{
    public get Provider(): Nullable<AnyProvider<T>>
    {
        return this._Provider;
    }

    public constructor(
        scope: Scope,
        token: Token<T>,
        private readonly _Provider: Nullable<AnyProvider<T>>,
        message: TemplateString<string>,
        messageTemplateValues: TemplateStringValues = {},
        innerException?: Error)
    {
        super(scope, token, message,
            Object.assign(messageTemplateValues, {
                provider: InjectionProvider.ToString(_Provider),
            }),
            innerException,
        );

        InjectionProviderException.setPrototype(this);
    }

    public toString(): string
    {
        let toString = super.toString();
        if (this._Provider) {
            let provider = InjectionProvider.ToString(this._Provider);
            if (this._Provider && toString.indexOf(provider) == -1)
                toString += `\nInjection Provider: ${provider}`;
        }
        return toString;
    }
}

export class UnknownInjectionProviderException<T> extends InjectionProviderException<T>
{
    public constructor(
        scope: Scope,
        token: Token<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(scope, token, undefined,
            TemplateString`The injection provider for token '${'token'}' could not be located within scope '${
                'scope'
            }' and its parents.`.Prepend(message), messageTemplateValues,
        );

        UnknownInjectionProviderException.setPrototype(this);
    }
}

export class UnknownInjectionProviderScopeException<T> extends InjectionProviderException<T>
{
    public constructor(scope: Scope, token: Token<T>, private provider: AnyProvider<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(scope, token, provider,
            TemplateString`The injection scope '${'scope'}' of provider '${'provider'}' for token '${
                'token'
            }' does not exist.`.Prepend(message),
            messageTemplateValues,
        );

        UnknownInjectionProviderScopeException.setPrototype(this);
    }
}

export class UnknownInjectionProviderTokenException<T> extends InjectionProviderException<T>
{
    public constructor(
        scope: Scope,
        token: Token<T>,
        private provider: AnyProvider<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(scope, token, provider,
            TemplateString`The injection token '${'token'}' of provider '${'provider'}' could not be located within scope '${
                'scope'
            }' and its parents.`.Prepend(message),
            messageTemplateValues,
        );

        UnknownInjectionProviderTokenException.setPrototype(this);
    }
}

export class DuplicateInjectionProviderException<T> extends InjectionProviderException<T>
{
    public constructor(
        scope: Scope,
        token: Token<T>,
        private provider: AnyProvider<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(scope, token, provider,
            TemplateString`The injection token '${'token'}' has already a provider '${'provider'}' within scope '${
                'scope'
            }' registered.`.Prepend(message),
            messageTemplateValues,
        );

        DuplicateInjectionProviderException.setPrototype(this);
    }
}
