import { TemplateString, TemplateStringValues } from '@awesome-nodes/object';
import { Scope } from 'factory/injection/InjectionScope';
import { InjectionScopeException } from 'factory/injection/InjectionScopeException';
import { InjectionToken, Token } from 'factory/injection/InjectionToken';


export class InjectionTokenException<T> extends InjectionScopeException
{
    public get Token(): Token<T>
    {
        return this._Token;
    }

    public constructor(
        scope: Scope,
        private readonly _Token: Token<T>,
        message: TemplateString<string>,
        messageTemplateValues: TemplateStringValues = {},
        innerException?: Error)
    {
        super(scope, message,
            Object.assign(messageTemplateValues, {
                token: InjectionToken.ToString(_Token),
            }),
            innerException,
        );

        InjectionTokenException.setPrototype(this);
    }

    public toString(): string
    {
        let toString = super.toString();
        let token    = InjectionToken.ToString(this._Token);
        if (this._Token && toString.indexOf(token) == -1)
            toString += `\nInjection Token: ${token}`;
        return toString;
    }
}

export class UnknownInjectionTokenException<T> extends InjectionTokenException<T>
{
    public constructor(
        scope: Scope,
        token: Token<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(scope, token,
            TemplateString`The injection token '${'token'}' could not be located within scope '${'scope'}'.`.Prepend(
                message),
            messageTemplateValues,
        );

        UnknownInjectionTokenException.setPrototype(this);
    }
}

export class UnknownInjectionTokenScopeException<T> extends InjectionTokenException<T>
{
    public constructor(
        scope: Scope,
        private token: Token<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(scope, token, TemplateString`The injection scope '${'scope'}' of token '${'token'}' does not exist.`
                .Prepend(message),
            messageTemplateValues,
        );

        UnknownInjectionTokenScopeException.setPrototype(this);
    }
}

export class DuplicateInjectionTokenException<T> extends InjectionTokenException<T>
{
    public constructor(
        scope: Scope,
        private token: Token<T>,
        message: TemplateString<string>,
        messageTemplateValues?: TemplateStringValues)
    {
        super(
            scope,
            token,
            TemplateString`The injection token '${'token'}' does already exists.`
                .Prepend(message),
            messageTemplateValues,
        );

        DuplicateInjectionTokenException.setPrototype(this);
    }
}
