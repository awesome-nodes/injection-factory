import {
    AbstractClass,
    ArgumentNullException,
    Enum,
    EnumValueType,
    isConstructor,
    isString,
    ObjectBase,
} from '@awesome-nodes/object';
import { InjectionScope } from 'factory/injection/InjectionScope';
import { ConstructorFunction } from 'simplytyped';


export type SimpleToken<T extends EnumValueType = EnumValueType> = string | symbol | Enum<T>
export type ObjectToken<T extends object = object> = ConstructorFunction<T> | AbstractClass<T>;
export type ReflectiveToken<T extends object = object> = SimpleToken | ObjectToken<T>;
export type KnownToken<T> = SimpleToken | InjectionToken<T>;
export type Token<T = unknown> = KnownToken<T> | ReflectiveToken;

export class InjectionToken<T> extends ObjectBase
{
    //region Public Properties

    public get Key(): SimpleToken
    {
        return this._Key;
    }

    public get Scope(): InjectionScope
    {
        return this._Scope;
    }

    public get Value(): T
    {
        return this._Scope.Inject(this._Key);
    }

    //endregion

    public constructor(private _Key: SimpleToken, private _Scope: InjectionScope)
    {
        super(String(_Key));
    }

    //region ObjectBase Overrides

    /** @inheritDoc */
    public toString(): string
    {
        return String(this._Key);
    }

    //endregion

    //region Static Conversion and Comparison Helpers

    public static Normalize<T>(token: Token): KnownToken<T>
    {
        if (!token)
            throw new ArgumentNullException('Normalizable injection tokens can not be empty. ', 'token');

        return (
            isString(token)
            || token instanceof InjectionToken
            || typeof token != 'function'
            || !isConstructor(token as AbstractClass<T>)
                ? token
                : token.name
        ) as KnownToken<T>;
    }

    public static Simplify(token: Token): SimpleToken
    {
        token = this.Normalize(token);
        return token instanceof InjectionToken
            ? token._Key
            : token;
    }

    public static Compare(token1: Token, token2: Token)
    {
        return this.Simplify(token1) == this.Simplify(token2);
    }

    //endregion

    //region Static Object Type (Function) Overrides

    public static ToString(token: Token)
    {
        return String(InjectionToken.Simplify(token));
    }

    //endregion
}

