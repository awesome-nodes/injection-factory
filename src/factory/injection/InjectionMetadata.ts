import { Scope } from 'factory/injection/InjectionScope';


export interface InjectionMetadata
{
    scope?: Scope
}
