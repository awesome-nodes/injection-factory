import { AbstractClass, isConstructor, isObject } from '@awesome-nodes/object';
import { AnyProvider, InjectableProvider, InjectionProvider } from 'factory/injection/InjectionProvider';
import { Scope } from 'factory/injection/InjectionScope';
import { SimpleToken, Token } from 'factory/injection/InjectionToken';
import { AnyFunc, ConstructorFunction } from 'simplytyped';


export type InjectionProviderConfig<T> =
    | ConstructorProviderConfig<T>
    | ClassProviderConfig<T>
    | ValueProviderConfig<T>
    | FactoryProviderConfig<T>
    | TokenProviderConfig<T>;

export type AnyInjectionProviderConfig<T> =
    & ConstructorProviderConfig<T>
    & ClassProviderConfig<T>
    & ValueProviderConfig<T>
    & FactoryProviderConfig<T>
    & TokenProviderConfig<T>

export interface BaseInjectionProviderConfig<T = unknown>
{
    /**
     * The injectable token the provider is related to for.
     */
    provide: Token<T>;
    /**
     * The injection scope to be used instead of the {@link InjectionFactory.MainScope}.
     */
    scope?: Scope;
    /**
     * A list of tokens to be resolved by the injection scope.
     */
    deps?: Array<Token>;
}

//region Class Provider

export interface ClassProviderConfig<T, TClass extends object = object> extends BaseInjectionProviderConfig<T>
{
    useClass: ConstructorFunction<TClass>;
}

export function isClassProvider<T>(provider: InjectableProvider<T>): provider is ClassProviderConfig<T>
{
    return (provider as AnyInjectionProviderConfig<T>).useClass !== undefined;
}

//endregion

//region Value Provider

export interface ValueProviderConfig<T, TValue = unknown> extends BaseInjectionProviderConfig<T>
{
    useValue: TValue;
}

export function isValueProvider<T>(provider: InjectableProvider<T>): provider is ValueProviderConfig<T>
{
    return (provider as AnyInjectionProviderConfig<T>).useValue !== undefined;
}

//endregion

//region Factory Provider

export interface FactoryProviderConfig<T, TFunc = AnyFunc<unknown>> extends BaseInjectionProviderConfig<T>
{
    useFactory: TFunc;
}

export function isFactoryProvider<T>(provider: InjectableProvider<T>): provider is FactoryProviderConfig<T>
{
    return (provider as AnyInjectionProviderConfig<T>).useFactory !== undefined;
}

//endregion

//region Token Provider

export interface TokenProviderConfig<T> extends BaseInjectionProviderConfig<T>
{
    provide: Token<T>;
    useToken: SimpleToken;
}

export function isTokenProvider<T>(provider: InjectableProvider<T>): provider is TokenProviderConfig<T>
{
    return (provider as AnyInjectionProviderConfig<T>).useToken !== undefined;
}

//endregion

//region Constructor Provider

export interface ConstructorProviderConfig<T, TProvide extends object = object> extends BaseInjectionProviderConfig<T>
{
    provide: ConstructorFunction<TProvide>
}

export function isConstructorProvider<T>(provider: InjectableProvider<T>): provider is ConstructorProviderConfig<T>
{
    return isObject(provider) && isConstructor((provider as AnyInjectionProviderConfig<T>).provide as AbstractClass<T>);
}

//endregion

export function isProviderConfig<T>(object: AnyProvider<T>): object is InjectionProviderConfig<T>
{
    return isObject(object) && !(object instanceof InjectionProvider) && (
        isConstructorProvider(object)
        || isClassProvider(object)
        || isValueProvider(object)
        || isFactoryProvider(object)
        || isTokenProvider(object));
}

