import { TemplateString, TemplateStringValues } from '@awesome-nodes/object';
import { Scope } from 'factory/injection/InjectionScope';
import { InjectionFactoryException } from 'factory/InjectionFactoryException';


export class InjectionScopeException extends InjectionFactoryException
{
    public get Scope(): Scope
    {
        return this._Scope;
    }

    public constructor(
        private readonly _Scope: Scope,
        message: TemplateString<string>,
        messageTemplateValues: TemplateStringValues = {},
        innerException?: Error)
    {
        super(message, Object.assign(messageTemplateValues, { scope: _Scope }), innerException);

        InjectionScopeException.setPrototype(this);
    }

    public toString(): string
    {
        let toString = super.toString();
        if (this._Scope && toString.indexOf(this._Scope.toString()) == -1)
            toString += `\nInjection Scope: ${this._Scope}`;
        return toString;
    }
}
