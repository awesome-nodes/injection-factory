import { ArgumentNullException, isString, ObjectBase, TemplateString } from '@awesome-nodes/object';
import InjectionFactory from 'factory';
import { InjectionProvider } from 'factory/injection/InjectionProvider';
import { InjectionProviderConfig, isProviderConfig } from 'factory/injection/InjectionProviderConfig';
import {
    DuplicateInjectionProviderException,
    UnknownInjectionProviderException,
    UnknownInjectionProviderTokenException,
} from 'factory/injection/InjectionProviderException';
import { InjectionToken, KnownToken, SimpleToken, Token } from 'factory/injection/InjectionToken';
import {
    DuplicateInjectionTokenException,
    UnknownInjectionTokenException,
} from 'factory/injection/InjectionTokenException';
import { convertFromProviderConfig } from 'factory/providers/ProviderConfig';
import { Nullable } from 'simplytyped';


export type Scope = string | InjectionScope;

export class InjectionScope extends ObjectBase
{
    public static get<U extends InjectionScope>(token: Token, parentScope?: Nullable<Scope>)
    {
        return InjectionFactory.CreateScope(String(InjectionToken.Simplify(token)), parentScope) as U;
    }

    //region Private Members

    private _Tokens    = new Array<InjectionToken<unknown>>();
    private _Providers = new Map<SimpleToken, InjectionProvider<unknown>>();

    //endregion

    //region Public Properties

    public get Tokens(): Array<InjectionToken<unknown>>
    {
        return this._Tokens.slice();
    }

    public get Providers(): Array<InjectionProvider<unknown>>
    {
        return Array.from(this._Providers.values());
    }

    public get Parent(): InjectionScope
    {
        return this._Parent as InjectionScope;
    }

    //endregion

    public constructor(name: string, private _Parent?: InjectionScope)
    {
        super(name);
    }

    //region Public Subscope Members

    /**
     * Adds a new subscope to this scope instance.
     * @param {string} scopeName
     * @returns {InjectionScope}
     */
    public AddSubscope(scopeName: string)
    {
        return InjectionFactory.CreateScope(scopeName, this);
    }

    //endregion

    //region Public Token Members

    /**
     * Locates the provided injection token within this scope instance.
     * @param {Token<T>} token
     * @param {InjectionScope} scope
     * @returns {Nullable<InjectionToken<T>>}
     */
    public LocateToken<T>(token: Token<T>, scope: InjectionScope = this): Nullable<InjectionToken<T>>
    {
        token    = InjectionToken.Normalize(token);
        let find = scope._Tokens.find(_token => _token.Key == token);
        return (!find && scope._Parent ? this.LocateToken(token, scope._Parent) : find) as InjectionToken<T>;
    }

    /**
     * Adds a new unique injection token to this scope instance.
     * @param {SimpleToken} tokens
     * @throws {@link DuplicateInjectionTokenException}
     * @returns {U}
     */
    public AddToken<T, U extends InjectionToken<T> | Array<InjectionToken<T>>>(...tokens: SimpleToken[]): U
    {
        for (const _newToken of tokens) {
            if (this._Tokens.find(_token => String(_token) == InjectionToken.ToString(_newToken)))
                throw new DuplicateInjectionTokenException(this, _newToken,
                    TemplateString`Can not register token '${'token'}' within scope '${'scope'}'. `,
                );
        }

        let injectionTokens = tokens.map(t => new InjectionToken<T>(t, this));
        this._Tokens.push(...injectionTokens);
        return injectionTokens.length == 1 ? <U>injectionTokens[0] : <U>injectionTokens;
    }

    //endregion

    //region Public Provider Members

    /**
     * Adds a new injection provider overrides an existing one within this scope instance.
     * @param {KnownToken<T>} token
     * @param {InjectionProvider<T>} provider
     * @param {boolean} override Indicates if the provider shall be overridden by the provided one.
     * @throws {@link ArgumentNullException} | {@link UnknownInjectionProviderTokenException} | {@link DuplicateInjectionProviderException}
     */
    public AddProvider<T>(
        token: KnownToken<T> | InjectionProviderConfig<T>,
        provider?: InjectionProvider<T>,
        override = false)
    {
        if (isProviderConfig(token as InjectionProviderConfig<T>)) {
            provider = convertFromProviderConfig(token as InjectionProviderConfig<T>, this);
            token    = provider.Token as KnownToken<T>;
        }
        else if (!provider)
            throw new ArgumentNullException(`Can not add provider'. `, 'provider');

        let injectionToken: InjectionToken<T>;
        if (!(token instanceof InjectionToken)) {
            if (!token || !isString(token) || !token.length)
                throw new ArgumentNullException(`Can not add provider '${provider.Name}'. `, 'token');

            injectionToken = this.LocateToken(token) as InjectionToken<T>;
            if (!injectionToken)
                throw new UnknownInjectionProviderTokenException(this, token, provider,
                    `Cannot add provider '${provider.Name}'. `,
                );
        }
        else injectionToken = token;

        if (!override && this._Providers.has(injectionToken.Key))
            throw new DuplicateInjectionProviderException(this, token, provider!,
                `Cannot override provider '${this._Providers.get(injectionToken.Key)!.Name}' with provider '${provider.Name}' of token '${injectionToken}'. `,
            );

        this._Providers.set(injectionToken.Key, provider);
        return provider;
    }

    //endregion

    /**
     * Performs a dependency injection task for the specified injection token.
     * @param {Token<T>} token
     * @param {T} defaultValue If specified, it will be returned immediately in the case of any lookup error.
     * @returns {T}
     * @throws {@link ArgumentNullException} | {@link UnknownInjectionTokenException} | {@link UnknownInjectionProviderException}
     */
    public Inject<T>(token: Token<T>, defaultValue?: T): T
    public Inject<T>(token: Token<T>, defaultValue?: T, scope?: this): T
    {
        let injectionToken: InjectionToken<T>;
        token = InjectionToken.Normalize(token);

        // Resolve injection token from provided token.
        if (token instanceof InjectionToken)
            injectionToken = token;
        else {
            injectionToken = this.LocateToken(token) as InjectionToken<T>;
            if (!injectionToken) {
                if (defaultValue) return defaultValue;
                throw new UnknownInjectionTokenException(scope || this, token, 'Dependency inject failed. ');
            }
        }

        // Resolve registered provider within this scope.
        let provider = this._Providers.get(injectionToken.Key) as InjectionProvider<T>;
        let value: Nullable<T>;

        if (!provider) {
            const injectionScopes = InjectionFactory.Scopes;
            let tokenScope        = injectionScopes.find((_scope: InjectionScope) =>
                _scope.Name == String(InjectionToken.Simplify(token)) ? _scope : undefined);
            if (!tokenScope)
                tokenScope = injectionScopes.find((_scope: InjectionScope) =>
                    _scope.Parent == this ? _scope : undefined);
            if (tokenScope && (provider = tokenScope._Providers.get(injectionToken.Key) as InjectionProvider<T>))
                value = provider.Provide();

            if (!provider && !this._Parent) {
                if (defaultValue) return defaultValue;
                throw new UnknownInjectionProviderException(scope || this, token, 'Dependency inject failed. ');
            }
        }
        else value = provider.Provide();

        // Bubble the injection scope chain up to locate a registered provider.

        if (!value && this._Parent)
            //@ts-ignore
            value = this._Parent.Inject(injectionToken, defaultValue, this);

        return value as T;
    }

    //region ObjectBase Overrides

    /** @inheritDoc */
    public toString(): string
    {
        let s = super.toString();
        if (this._Parent)
            s = `${this!._Parent.toString()}/${s}`;
        return s;
    }

    //endregion

    //region Static Object Type (Function) Overrides

    /** @inheritDoc */
    public static ToString(scope: Scope, parent: Nullable<Scope> = InjectionFactory.MainScope)
    {
        return (isString(scope) && scope.indexOf('/') == -1 && parent != null)
            ? `${parent}/${scope}`
            : String(scope);
    }

    //endregion
}
