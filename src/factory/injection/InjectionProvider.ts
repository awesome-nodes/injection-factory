import { ArgumentNullException, isString, ObjectBase } from '@awesome-nodes/object';
import InjectionFactory from 'factory';
import { InjectionProviderConfig } from 'factory/injection/InjectionProviderConfig';
import { UnknownInjectionProviderScopeException } from 'factory/injection/InjectionProviderException';
import { InjectionScope, Scope } from 'factory/injection/InjectionScope';
import { InjectionToken, Token } from 'factory/injection/InjectionToken';
import { setProviderMetadata } from 'factory/Provider.decorator';
import { ConstructorFunction, Nullable } from 'simplytyped';


export type DependencyProvider<T> = InjectionProvider<T> | object
export type DependencyProviderType<T> = ConstructorFunction<DependencyProvider<T>>;
export type InjectableProvider<T> = DependencyProviderType<T> | InjectionProviderConfig<T>;
export type AnyProvider<T> =
    ConstructorFunction<InjectionProvider<T>>
    | InjectionProvider<T>
    | InjectionProviderConfig<T>

export class InjectionProvider<T> extends ObjectBase
{
    protected _Scope: InjectionScope;

    //region Public Properties

    public get Scope(): InjectionScope
    {
        return this._Scope;
    }

    public get Token(): Token
    {
        return this._Token;
    }

    public get Value(): T
    {
        return this._Value as T;
    }

    //endregion

    public constructor(scope: Scope, private _Token: Token, protected _Value: Nullable<T>)
    {
        super();

        if (!scope)
            throw new ArgumentNullException(
                `Can not instantiate provider '${this.Name}' for token '${InjectionToken.ToString(_Token)}'. `,
                'scope',
            );

        let injectionScope: InjectionScope;
        if (isString(scope)) {
            if (!scope.length)
                throw new ArgumentNullException(`Can not instantiate provider '${this.Name}' `, 'scope');
            injectionScope = InjectionFactory.Scopes.find(_scope => _scope.toString() == scope) as InjectionScope;
        }
        else injectionScope = scope;

        if (!injectionScope)
            throw new UnknownInjectionProviderScopeException(scope, _Token, this,
                `Can not instantiate provider '${this.Name}' `,
            );

        this._Scope = injectionScope;

        setProviderMetadata(this.constructor as ConstructorFunction<this>, _Token);
    }

    public Provide(): T
    {
        return this._Value as T;
    }

    //region Static Object Type (Function) Overrides

    public static ToString<T>(provider: Nullable<AnyProvider<T>>): string
    {
        return typeof provider == 'function'
            ? provider.name
            : (provider instanceof InjectionProvider ? provider.Name : String(provider));
    }

    //endregion
}
