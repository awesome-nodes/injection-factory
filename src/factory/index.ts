export * from 'factory/InjectionFactoryException';
export * from 'factory/Inject.decorator';
export * from 'factory/Injectable.decorator';
export * from 'factory/Provider.decorator';

import InjectionFactory from 'factory/InjectionFactory';


export default InjectionFactory;
